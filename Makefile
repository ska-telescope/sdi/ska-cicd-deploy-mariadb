INVENTORY_FILE ?= ./inventory_mariadb_cluster##inventory file to be generated
PRIVATE_VARS ?= ./mariadb_cluster_vars.yml##template variable for heat-cluster
ANSIBLE_USER ?= ubuntu## ansible user for the playbooks
CLUSTER_KEYPAIR ?= piers-engage## key pair available on openstack to be put in the created VMs
DEBUG ?= false
CONTAINERD ?= true
NVIDIA ?= false
IGNORE_NVIDIA_FAIL ?= false
TAGS ?=
EXTRA_VARS ?=
COLLECTIONS_PATHS ?= ./collections
V ?=

MYSQL_ROOT_PASSWORD?=secret## MySQL root user password
MYSQL_DATABASE?=tango## Database created by default when playbook is run
MYSQL_USER?=eda_admin## Username of MySQL Admin user to be distributed to the team using the database
MYSQL_PASSWORD?=another_secret## Additional Admin user's password
MYSQL_PRIVILEGES?='*.*:ALL,GRANT'## Privileges granted to the Admin user

.DEFAULT_GOAL := help

.PHONY: check_production

# define overides for above variables in here
-include PrivateRules.mak

ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
STACK_CLUSTER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/stack_cluster/playbooks
DOCKER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/playbooks

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "PRIVATE_VARS=$(PRIVATE_VARS)"
	@echo "MYSQL_ROOT_PASSWORD=$(MYSQL_ROOT_PASSWORD)"
	@echo "CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "EXTRA_VARS=$(EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "STACK_CLUSTER_PLAYBOOKS=$(STACK_CLUSTER_PLAYBOOKS)"
	@echo "DOCKER_PLAYBOOKS=$(DOCKER_PLAYBOOKS)"
	@echo "CONTAINERD=$(CONTAINERD)"
	@echo "NVIDIA=$(NVIDIA)"
	@echo "IGNORE_NVIDIA_FAIL=$(IGNORE_NVIDIA_FAIL)"
	@echo "DEBUG=$(DEBUG)"

uninstall:  # uninstall collections
	rm -rf $(COLLECTIONS_PATHS)/ansible_collections/*

install:  ## Install dependent ansible collections
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-galaxy collection install \
	-r requirements.yml -p ./collections

reinstall: uninstall install ## reinstall collections

all: build

check_production:
	(grep 'production: true' $(PRIVATE_VARS) && (echo "$(INVENTORY_FILE)" | grep "k8s_system") ) || \
	(grep 'production: false' $(PRIVATE_VARS) && (echo "$(INVENTORY_FILE)" | grep -v "k8s_system") ) || \
	(printf "aborting, as this is not production ($(PRIVATE_VARS)) \n"; exit 1)

build_vms: check_production ## Build vms only based on heat-cluster
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="cluster_keypair=$(CLUSTER_KEYPAIR)" \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
	--extra-vars="inventory_file=../../../../$(INVENTORY_FILE)" $(V)

build_nodes: check_production ## Build nodes based on heat-cluster
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="cluster_keypair=$(CLUSTER_KEYPAIR)" \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
	--extra-vars="inventory_file=../../../../$(INVENTORY_FILE)" $(V)
	make build_common
	make build_docker

build_common:  ## apply the common roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(PRIVATE_VARS) $(V)

build_docker:  ## apply the docker roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(PRIVATE_VARS) $(V)

clean_nodes:  ## destroy the nodes - CAUTION THIS DELETES EVERYTHING!!!
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/remove-cluster-infra.yml $(V)

clean: clean_nodes  ## destroy the nodes - CAUTION THIS DELETES EVERYTHING!!!

check_nodes: check_production ## Check nodes based on heat-cluster
	ansible -i ./$(INVENTORY_FILE) cluster \
	                         -m shell -a 'df; lsblk'

lint: reinstall ## Lint check playbook
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-lint -p playbooks/setup.yml \
				 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
				 $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
				 $(DOCKER_PLAYBOOKS)/docker.yml \
				 $(V) > ansible-lint-results.txt; \
	cat ansible-lint-results.txt

build_mariadb: check_production ## Build mariadb server
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(INVENTORY_FILE) playbooks/setup.yml \
	-e @$(PRIVATE_VARS) \
	--extra-vars="DOCKER_REGISTRY_HOST=$(DOCKER_REGISTRY_HOST) DOCKER_REGISTRY_PASSWORD=$(DOCKER_REGISTRY_PASSWORD) DOCKER_REGISTRY_USERNAME=$(DOCKER_REGISTRY_USERNAME)" \
	--extra-vars="mysql_root_password=$(MYSQL_ROOT_PASSWORD) mysql_database=$(MYSQL_DATABASE) mysql_user=$(MYSQL_USER) mysql_password=$(MYSQL_PASSWORD) mysql_privileges=$(MYSQL_PRIVILEGES)" \
	--extra-vars="activate_containerd=$(CONTAINERD) activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) debug=$(DEBUG)" $(V)

maria: build_mariadb

build: build_nodes build_mariadb  ## Build vm and mariadb

test: ## Smoke test for new created cluster
	ansible-playbook -i $(INVENTORY_FILE) playbooks/test.yml -e @$(PRIVATE_VARS) -vvv

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
